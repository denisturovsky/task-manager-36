package ru.tsc.denisturovsky.tm.dto.request;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;

@NoArgsConstructor
public final class DataRemoveRequest extends AbstractUserRequest {

    public DataRemoveRequest(@Nullable String token) {
        super(token);
    }

}
