package ru.tsc.denisturovsky.tm.sevice;

import org.jetbrains.annotations.NotNull;
import ru.tsc.denisturovsky.tm.api.repository.ISessionRepository;
import ru.tsc.denisturovsky.tm.api.service.ISessionService;
import ru.tsc.denisturovsky.tm.model.Session;

public final class SessionService extends AbstractUserOwnedService<Session, ISessionRepository> implements ISessionService {

    public SessionService(@NotNull final ISessionRepository repository) {
        super(repository);
    }

}